﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace coffee_shops {
   internal class Program {
      public static void Main(string[] args) {
         Execution execution = new Execution();
         execution.Execute();
      }
   }

   public class Execution {
      public void SetData() {
      }

      public void Execute() {
         CoffeeList coffeeList;
         string output;
         
         Coffee coffee = new Coffee {
            name = "Quang",
            address = "92 vo thi sau",
            district = HanoiDistrict.HaiBaTrung
         };

         using (StreamReader file = new StreamReader($"data.json")) {
            string json = file.ReadToEnd();
            // Console.WriteLine($"json: {json}");

            coffeeList = JsonConvert.DeserializeObject<CoffeeList>(json);

            if (coffeeList == null) {
               Console.WriteLine($"Coffee list null");
               coffeeList = new CoffeeList();
            }
            
            coffeeList.Coffees.Add(coffee);
            
            Console.WriteLine($"Serialize: {JsonConvert.SerializeObject(coffeeList)}");


            output = JsonConvert.SerializeObject(coffeeList).JsonPrettify();
            Console.WriteLine($"output: {output}");
         }

         File.WriteAllText("data.json", output);
      }
   }

   [Serializable]
   public class CoffeeList {
      public List<Coffee> Coffees;

      public CoffeeList() {
         Coffees = new List<Coffee>();
      }
   }

   [Serializable]
   public class Coffee {
      public string name;
      public string address;
      public string locationLink;
   }

   public static class JsonUtilities {
      public static string JsonPrettify(this string json) {
         using (var stringReader = new StringReader(json))
         using (var stringWriter = new StringWriter()) {
            var jsonReader = new JsonTextReader(stringReader);
            var jsonWriter = new JsonTextWriter(stringWriter) { Formatting = Formatting.Indented };
            jsonWriter.WriteToken(jsonReader);
            return stringWriter.ToString();
         }
      }
   }

   [Serializable]
   public enum HanoiDistrict {
      HaiBaTrung,
      TayHo
   }
}